# HTML parser written in pure Rust,no-std
[![Crates.io](https://img.shields.io/crates/v/loa.svg)](https://crates.io/crates/loa)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/loa)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/loa/-/raw/master/LICENSE)
```
[dependencies]
loa = "0.1.3"
```

```rust
fn main() {
    let html = include_str!("../index.html");
    use loa::get_elements_by_tag_name;
    use loa::Getattribute;
    let a_vec: Vec<String> = get_elements_by_tag_name(html, "a");
    if let Some(next) = a_vec
        .iter()
        .filter(|s| s.contains("Next page"))
        .collect::<Vec<_>>()
        .get(0)
    {
        let href = next.get_attribute("href");
        let class = next.get_attribute("class");
        println!("{}", href);
        println!("{}", class);
    }
}

```