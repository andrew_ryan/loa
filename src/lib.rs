#![no_std]
extern crate alloc;
use alloc::{string::ToString, vec::Vec};
use alloc::string::String;

pub use parse::get_elements_by_tag_name;
/// get tag attributes
/// ```rust
///fn parse() {
///     let html = include_str!("../index.html");
///     use loa::get_elements_by_tag_name;
///     use loa::Getattribute;
///     let a_vec:Vec<String> = get_elements_by_tag_name(html, "a");
///     if let Some(next) = a_vec
///         .iter()
///         .filter(|s| s.contains("Runloop"))
///         .collect::<Vec<_>>()
///         .get(0)
///     {
///         let href = next.get_attribute("href");
///         let class = next.get_attribute("class");
///         println!("{}", href);
///         println!("{}", class);
///     }
/// }
/// ```
pub trait Getattribute {
    fn get_attribute(&self, attr: impl ToString) -> String;
}
#[allow(warnings)]
impl Getattribute for String {
    fn get_attribute(&self, attr:impl ToString) -> String {
        let attr = attr.to_string();
        let out_tag = self.split(" ").filter(|s|!s.is_empty())
        .collect::<Vec<_>>().get(0).expect("error to get out tag").to_string().replace("<", "");
        let new_self_vec = self.split("><").filter(|s|!s.is_empty())
        .map(|s|s.to_string()).collect::<Vec<_>>();
        let new_self = new_self_vec.get(0).unwrap_or(&self).replace("\"\"", "\"loa_eloon_tesla_spacex_boring\"");
        let attr_vec = new_self.split("\"").filter(|s|!s.is_empty())
        .map(|s|{s.trim().to_string()})
        .collect::<Vec<_>>();
        let mut attr_index:usize = 0;
        for i in 0..attr_vec.len(){
            let s = attr_vec.get(i).expect("get error");
                if s.contains(&attr){
                    attr_index+=1;
                    break;
                }
            attr_index = attr_index+1;
        };
        attr_vec.get(attr_index).expect("get attr error").to_string()
    }
}
#[allow(warnings)]
mod parse{
    use alloc::{string::ToString, vec::Vec, fmt::format};
    use libc_print::std_name::{println, eprintln, dbg};
    use alloc::string::String;

    /// parse html get Vec of tags
    /// ```rust
    ///fn parse() {
    ///     let html = include_str!("../index.html");
    ///     use loa::get_elements_by_tag_name;
    ///     use loa::Getattribute;
    ///     let a_vec:Vec<String> = get_elements_by_tag_name(html, "a");
    ///     if let Some(next) = a_vec
    ///         .iter()
    ///         .filter(|s| s.contains("Runloop"))
    ///         .collect::<Vec<_>>()
    ///         .get(0)
    ///     {
    ///         let href = next.get_attribute("href");
    ///         let class = next.get_attribute("class");
    ///         println!("{}", href);
    ///         println!("{}", class);
    ///     }
    /// }
    /// ```
    pub fn get_elements_by_tag_name(html:impl ToString,tag:impl ToString)->Vec<String>{
        let html = html.to_string();
        let a_b = html.split(format(format_args!("</{}>", tag.to_string())).as_str()).filter(|s|!s.is_empty()).filter(|s|{
            s.contains(format(format_args!("<{} ", tag.to_string())).as_str())
        }).map(|s|{
            let a_e = s.replace("\n", "").trim().to_string();
            let a_v = a_e.split(format(format_args!("<{} ", tag.to_string())).as_str()).map(|s|{
                s.to_string()
            }).collect::<Vec<_>>();
            let mut a = a_v.get(1).unwrap().to_string();
            a.push_str(format(format_args!("</{}>", tag.to_string())).as_str());
            let mut aa = String::from(format(format_args!("<{} ", tag.to_string())).as_str());
            aa.push_str(&a);
            aa
        }).collect::<Vec<_>>();
        a_b
    }
    


}

